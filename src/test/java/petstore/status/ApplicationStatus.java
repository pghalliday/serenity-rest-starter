package starter.status;

import io.restassured.RestAssured;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

import static starter.WebServiceEndPoints.PET_BY_ID;

public class ApplicationStatus {

    public AppStatus currentStatus() {
        int statusCode = RestAssured.get(PET_BY_ID.getUrl("100")).statusCode();
        return (statusCode == 200) ? AppStatus.RUNNING : AppStatus.DOWN;
    }

    @Step("Get current status message")
    public void readStatusMessage() {
        SerenityRest.get(PET_BY_ID.getUrl("100"));
    }
}
