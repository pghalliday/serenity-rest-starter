package starter;

import java.util.Map;
import java.util.HashMap;
import org.apache.commons.text.StringSubstitutor;

public enum WebServiceEndPoints {

    PET_UPLOAD_IMAGE("https://petstore.swagger.io/v2/pet/${0}/uploadImage"),
    PET("https://petstore.swagger.io/v2/pet"),
    PET_FIND_BY_STATUS("https://petstore.swagger.io/v2/pet/findByStatus"),
    PET_FIND_BY_TAGS("https://petstore.swagger.io/v2/pet/findByTags"),
    PET_BY_ID("https://petstore.swagger.io/v2/pet/${0}"),

    STORE_ORDER("https://petstore.swagger.io/v2/store/order"),
    STORE_ORDER_BY_ID("https://petstore.swagger.io/v2/store/order/${0}"),
    STORE_INVENTORY("https://petstore.swagger.io/v2/store/inventory"),

    USER("https://petstore.swagger.io/v2/user"),
    USER_BY_NAME("https://petstore.swagger.io/v2/user/${0}"),
    USER_LOGIN("https://petstore.swagger.io/v2/user/login"),
    USER_LOGOUT("https://petstore.swagger.io/v2/user/logout"),
    USER_CREATE_WITH_ARRAY("https://petstore.swagger.io/v2/user/createWithArray"),
    USER_CREATE_WITH_LIST("https://petstore.swagger.io/v2/user/createWithList");

    private final String url;

    WebServiceEndPoints(String url) {
        this.url = url;
    }

    // Although we only need to substitute at most one field in our
    // URLs, for the sake of future proofing this utility code we will
    // support an arbitrary number of fields
    public String getUrl(String ...fields) {
        Map<String, String> fieldsMap = new HashMap<String, String>();
        int fieldIndex = 0;
        for (String field: fields) {
          fieldsMap.put(String.valueOf(fieldIndex), field);
          fieldIndex++;
        }
        StringSubstitutor sub = new StringSubstitutor(fieldsMap);
        return sub.replace(url);
    }
}
